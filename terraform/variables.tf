variable "aws_profile" {
  description = "AWS profile used to deploy AWS resources"
}

variable "aws_region" {
  description = "AWS region used to deploy AWS resources"
  default     = "eu-west-1"
}

variable "app_name" { default = "blueprint-ssm" }

provider "aws" {
  profile = var.aws_profile
  region  = var.aws_region
}


# -----------------------------------------------------------------------------------
# IAM Resources: A role for the instance to use ssm
# -----------------------------------------------------------------------------------
resource "aws_iam_instance_profile" "ssm_managed_instance" {
  name = "${var.app_name}-ssm-managed"
  role = aws_iam_role.ec2_role.name
}
resource "aws_iam_role" "ec2_role" {
  name               = "${var.app_name}-ssm-managed"
  description        = "Role assigned to EC2 Instance"
  assume_role_policy = data.template_file.ec2_assume_role.rendered
}
resource "aws_iam_role_policy_attachment" "ssm-instance-core-attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


# -----------------------------------------------------------------------------------
# IAM Resources: A group for the users who connect to instance
# -----------------------------------------------------------------------------------

resource "aws_iam_group" "ssm_users" {
  name = "${var.app_name}-shell-users"
}

resource "aws_iam_group_policy" "my_developer_policy" {
  name   = "allow_ssm_on_specific_instances"
  group  = aws_iam_group.ssm_users.id
  policy = data.template_file.allow_ssm_on_instances.rendered
}


# -----------------------------------------------------------------------------------
# EC2 Instance
# -----------------------------------------------------------------------------------
resource "aws_instance" "ssm_demo" {
  ami           = data.aws_ami.amzn_linux2.id
  instance_type = "t2.micro"

  iam_instance_profile = aws_iam_instance_profile.ssm_managed_instance.name

  user_data = file("scripts/user-data.sh")

  tags = {
    Name = "${var.app_name}-instance"
  }
}

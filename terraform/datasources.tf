data "aws_ami" "amzn_linux2" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami*x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["137112412989"]
}

/* get info about current account */
data "aws_caller_identity" "current" {}

data "template_file" "ec2_assume_role" {
  template = "${file("${path.module}/policies/ec2_assume_policy.tpl")}"
}

data "template_file" "allow_ssm_on_instances" {
  template = "${file("${path.module}/policies/allow_ssm_on_instances_policy.tpl")}"
  vars = {
    account-id  = data.aws_caller_identity.current.account_id
    region      = var.aws_region
    instance-id = aws_instance.ssm_demo.id
  }
}

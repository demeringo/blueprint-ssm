# Blueprint-SSM

Allow users to open a terminal on a Linux instance without SSH key, using AWS Session Manager.

## Usage

### Create the instance and permissions

```sh
cd terraform
terraform init
terraform plan
terraform apply
```

### Optional: authorize a non admin user access

If the user has a non-administrative account, add it to the `blueprint-ssm-shell-users` group.

### Open shell on the instance from web browser (aws console)

1. Select the instance from the EC2 page and click on the `connect` button on top.
1. Select `SSM access` in the dialog box and `connect`

### Open shell on the instance from command line (aws CLI)

#### Install SSM plugin for CLI

https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html

#### Connect to instance

```sh
#aws ssm start-session --target id-of-an-instance-you-have-permissions-to-access
# example with instance id i-0b6d9ec889ff2036f
aws ssm start-session --target i-0b6d9ec889ff2036f --profile myadminprofile

# With non admin user the explicit document-name parameter is required by the policy
aws ssm start-session --target i-0bd894e92c00ed7d4 --profile mybasicprofile --document-name SSM-SessionManagerRunShell

```

#### Optional: connect to instance by name or tag

Install aws-connect (see <https://github.com/rewindio/aws-connect>) to use a nice wrapper script that allow to access instances by name or tag.

```sh
# Connect to an instance whose Name tag is blueprint-ssm-instance
# with -s parameter, you are prompted for choice if multiple instance match
# -r and -p for region and profile
./aws-connect -s -n blueprint-ssm-instance -p myprofile -r eu-west-1

# Connect to an instance with specific tag
aws-connect -s -t CLUSTER=prod
```

## Why use AWS Session Manager

Session manager allows developers to access EC2 instances without using SSH directly.

* avoid distributing SSH keys to the team member
* manage access permissions through IAM groups (using tags or instance id's).
* replace a bastion host (to access instance in private subnets)
* _Optional_: Access or commands can be logged (to S3 or cloudwatch) for audit or compliance.
* _Optional_: a standard SSH session can be tunneled through the SSM session (for example to to use scp)

## Principle

* An agent runs on the instance (AWS linux includes it by default)
* Instance is configured with a role that allows connection of the agent to AWS Session Manager (policy `AmazonSSMManagedInstanceCore`)
* EC2 admin users can now open a session on the instance from the AWS console or from the CLI.
* Optional: we can use IAM permissions to restrict or allow which instance a non admin user can start sessions (or which actions can be performed).

## Limitations of the blueprint

* No `scp` functionality (unless you set up SSH tunnel which will require SSH key in addition to SSM).
* No Audit log activated.

## References

* https://kscherer.github.io/aws/2019/11/07/using-aws-session-manager-to-connect-to-machines-in-a-private-subnet
* https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-getting-started.html
* https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-getting-started-restrict-access.html
* https://compliance.dev/2020/03/20/aws-session-manager-less-infrastructure-more-features/
* Policies to [manage permissions on specific instances](https://docs.aws.amazon.com/systems-manager/latest/userguide/getting-started-restrict-access-quickstart.html)
* Other example of policies (with TF) https://github.com/symopsio/terraform-okta-ssm-modules
